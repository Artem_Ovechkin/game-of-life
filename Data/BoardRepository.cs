﻿using System.Collections.Generic;
using System.IO;
using Conways.GameOfLife.Game;

namespace Conways.GameOfLife.Data
{
	public class BoardRepository : IBoardRepository
	{
		private const string path = @"Assets";

		public void SaveBoard(string fileName, HashSet<Position> liveCells)
		{
			if (!Directory.Exists(path))
				Directory.CreateDirectory(path);

			var file = File.CreateText(Path.Combine(path, fileName));

			foreach (var cell in liveCells)
			{
				file.Write(cell.ToString());
			}

			file.Flush();
			file.Close();
		}

		public HashSet<Position> LoadBoard(string fileName)
		{
			var result = new HashSet<Position>();
			
			if (!File.Exists(Path.Combine(path, fileName)))
				return new HashSet<Position>();

			var file = File.OpenText(Path.Combine(path, fileName));

			var text = file.ReadToEnd();

			file.Close();

			foreach (var coords in text.Split(';'))
			{
				if (string.IsNullOrEmpty(coords))
					continue;
				var split = coords.Split(',');
				result.Add(new Position(int.Parse(split[0]), int.Parse(split[1])));
			}

			return result;
		}
	}
}
