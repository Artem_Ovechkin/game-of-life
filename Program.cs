﻿using System;
using System.Windows;
using Conways.GameOfLife.Game;

namespace Conways.GameOfLife
{
	class Program
	{
		[STAThread]
		static void Main()
		{
			new Application();
			Window wnd = new MainWindow();
			wnd.Show();
			Application.Current.Run();
		}
	}
}
