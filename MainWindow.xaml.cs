﻿using Conways.GameOfLife.Game;
using Conways.GameOfLife.Infrastructure;
using Conways.GameOfLife.Interfaces;
using Conways.GameOfLife.Model;

namespace Conways.GameOfLife
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : IBoardView
	{
		public MainWindow()
		{
			InitializeComponent();

			ViewModel = new BoardViewModel(this, view);
			DataContext = ViewModel;
		}

		public IBoardViewModel ViewModel { get; set; }
	}
}
