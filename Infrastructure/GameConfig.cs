﻿using System;
using System.IO;
using System.Threading;
using System.Xml;
using System.Xml.Serialization;

namespace Conways.GameOfLife.Game
{
	[Serializable]
	public class GameConfig
	{
		private const string FileName = "GameConfig.xml";
		private static readonly object LockFlag = new object();
		private static GameConfig _instance;

		public GameConfig()
		{
			VisibleBoardSizeX = 100;
			VisibleBoardSizeY = 100;
			CellSize = 5;
		}

		public int VisibleBoardSizeX { get; set; }
		public int VisibleBoardSizeY { get; set; }
		public int CellSize { get; set; }

		public static GameConfig Instance
		{
			get
			{
				if (_instance == null)
				{
					lock (LockFlag)
					{
						if (_instance == null)
						{
							var gameConfig = Load() ?? new GameConfig();
							Thread.MemoryBarrier();
							_instance = gameConfig;
						}
					}
				}

				return _instance;
			}
		}

		private static GameConfig Load()
		{
			lock (LockFlag)
			{
				try
				{
					if (File.Exists(FileName))
					{
						using (var fileStream = new FileStream(FileName, FileMode.Open, FileAccess.Read, FileShare.Read))
						{
							var xmlSerializer = new XmlSerializer(typeof(GameConfig));
							return (GameConfig)xmlSerializer.Deserialize(fileStream);
						}
					}
				}
				catch (Exception ex)
				{
					System.Diagnostics.Trace.TraceError("Error in GetConfigFromFile() : {0}", ex.Message);
				}

				return null;
			}
		}

		public void Save()
		{
			lock (LockFlag)
			{
				using (var fileStream = new FileStream(FileName, FileMode.Create))
				{
					var xmlSerializer = new XmlSerializer(typeof(GameConfig));
					var ns = new XmlSerializerNamespaces();
					ns.Add("", "");

					using (
						var xmlWriter = XmlWriter.Create(fileStream, new XmlWriterSettings { OmitXmlDeclaration = true, Indent = true }))
					{
						xmlSerializer.Serialize(xmlWriter, this, ns);
					}
				}
			}
		}
	}

	public interface IConfigProvider
	{
		GameConfig LoadConfig();
	}

	public class ConfigProvider : IConfigProvider
	{
		public GameConfig LoadConfig()
		{
			return GameConfig.Instance;
		}
	}
}
