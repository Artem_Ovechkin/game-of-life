﻿using System;

namespace Conways.GameOfLife.Infrastructure
{
	public static class RandomHelper
	{
		public static long LongRandom(long min, long max, Random rand)
		{
			var buf = new byte[8];
			rand.NextBytes(buf);
			var longRand = BitConverter.ToInt64(buf, 0);

			return (Math.Abs(longRand % (max - min)) + min);
		}
	}
}
