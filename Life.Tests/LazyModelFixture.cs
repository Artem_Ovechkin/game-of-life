﻿using System;
using Conways.GameOfLife.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Conways.GameOfLife.Game;

namespace Life.Tests
{
	[TestClass]
	public class LazyModelFixture
	{
		[TestMethod]
		public void FewerThanTwoDies_0()
		{
			var model = new LifeEngine();

			model.ToggleCell(0,0);

			model.Next();

			Assert.IsFalse(model.CellAlive(0, 0));
		}

		[TestMethod]
		public void FewerThanTwoDies_1()
		{
			var model = new LifeEngine();

			model.ToggleCell(0, 0);
			model.ToggleCell(0, 1);

			model.Next();

			Assert.IsFalse(model.CellAlive(0, 0));
		}

		[TestMethod]
		public void TwoLiveLives()
		{
			var model = new LifeEngine();

			model.ToggleCell(0, 0);
			model.ToggleCell(0, 1);
			model.ToggleCell(1, 0);

			model.Next();

			Assert.IsTrue(model.CellAlive(0, 0));
		}

		[TestMethod]
		public void ThreeLiveLives()
		{
			var model = new LifeEngine();

			model.ToggleCell(0, 0);
			model.ToggleCell(0, 1);
			model.ToggleCell(1, 0);
			model.ToggleCell(1, 1);

			model.Next();

			Assert.IsTrue(model.CellAlive(0, 0));
		}

		[TestMethod]
		public void MoreThanThreeDies()
		{
			var model = new LifeEngine();

			model.ToggleCell(0, 0);
			model.ToggleCell(0, 1);
			model.ToggleCell(1, 0);
			model.ToggleCell(1, 1);
			model.ToggleCell(1, -1);

			model.Next();

			Assert.IsFalse(model.CellAlive(0, 0));
		}

		[TestMethod]
		public void MoreThanThreeDies_4()
		{
			var model = new LifeEngine();

			model.ToggleCell(0, 0);
			model.ToggleCell(0, 1);
			model.ToggleCell(1, 0);
			model.ToggleCell(1, 1);
			model.ToggleCell(1, -1);
			model.ToggleCell(-1, -1);

			model.Next();

			Assert.IsFalse(model.CellAlive(0, 0));
		}

		[TestMethod]
		public void MoreThanThreeLiveNotBorn()
		{
			var model = new LifeEngine();

			model.ToggleCell(0, 1);
			model.ToggleCell(1, 0);
			model.ToggleCell(1, 1);
			model.ToggleCell(1, -1);
			model.ToggleCell(-1, -1);

			model.Next();

			Assert.IsFalse(model.CellAlive(0, 0));
		}

		[TestMethod]
		public void ThreeLiveBorn()
		{
			var model = new LifeEngine();

			model.ToggleCell(0, 1);
			model.ToggleCell(1, 0);
			model.ToggleCell(1, 1);

			model.Next();

			Assert.IsTrue(model.CellAlive(0, 0));
		}

	}
}