﻿using Conways.GameOfLife.Interfaces;

namespace Conways.GameOfLife.Infrastructure
{
	public interface IView
	{
		IBoardViewModel ViewModel { get; set; }
	}
}
