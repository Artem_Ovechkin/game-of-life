﻿using System.Collections.Generic;
using Conways.GameOfLife.Game;

namespace Conways.GameOfLife.Data
{
	public interface IBoardRepository
	{
		void SaveBoard(string fileName, HashSet<Position> liveCells);
		HashSet<Position> LoadBoard(string fileName);
	}
}
