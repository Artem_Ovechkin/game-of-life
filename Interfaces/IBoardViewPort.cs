﻿namespace Conways.GameOfLife.Infrastructure
{
	public interface IBoardViewPort : IView
	{
		void Update();
	}
}
