﻿using System.Collections.Generic;
using Conways.GameOfLife.Game;
using Conways.GameOfLife.Infrastructure;

namespace Conways.GameOfLife.Interfaces
{
	public interface IBoardViewModel
	{
		IBoardView BoardView { get; set; }
		long XOffset { get; set; }
		long YOffset { get; set; }

		long CurrentCellX{ get; set; }
		long CurrentCellY { get; set; }

		int ViewPortWidth { get; set; }
		int ViewPortHeight { get; set; }

		HashSet<Position> LiveCells { get; set; }
		
		void Start();
		void Stop();
		void Clear();
		void Randomize();

		bool IsActive { get; set; }

		void Next();
		void ToggleCell(long x, long y);
	}
}
