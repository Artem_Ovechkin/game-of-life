﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Media;
using Conways.GameOfLife.Annotations;
using Conways.GameOfLife.Data;
using Conways.GameOfLife.Game;
using Conways.GameOfLife.Infrastructure;
using Conways.GameOfLife.Interfaces;
using Microsoft.Practices.Prism.Commands;

namespace Conways.GameOfLife.Model
{
	public class BoardViewModel : IBoardViewModel, INotifyPropertyChanged
	{
		public DelegateCommand StartStopCommand { get; set; }
		public DelegateCommand NextCommand { get; set; }
		public DelegateCommand RandomizeCommand { get; set; }
		public DelegateCommand ClearCommand { get; set; }
		public DelegateCommand LoadCommand { get; set; }
		public DelegateCommand SaveCommand { get; set; }

		public DelegateCommand TopLeftCommmand { get; set; }
		public DelegateCommand TopRightCommmand { get; set; }
		public DelegateCommand BottomLeftCommmand { get; set; }
		public DelegateCommand BottomRightCommmand { get; set; }
		public DelegateCommand CenterCommmand { get; set; }
		public DelegateCommand RandomCellCommmand { get; set; }

		private BoardRepository repository;


		private LifeEngine engine = new LifeEngine();
		
		public BoardViewModel(IBoardView boardView, IBoardViewPort viewPort)
		{
			BoardView = boardView;
			BoardViewPort = viewPort;
			viewPort.ViewModel = this;
			engine.Update += BoardViewModel_Update;

			StartStopCommand = new DelegateCommand(StartStop);
			NextCommand = new DelegateCommand(Next);
			RandomizeCommand = new DelegateCommand(Randomize);
			ClearCommand = new DelegateCommand(Clear);
			LoadCommand = new DelegateCommand(LoadState);
			SaveCommand = new DelegateCommand(SaveState);

			TopLeftCommmand = new DelegateCommand(TopLeft);
			TopRightCommmand = new DelegateCommand(TopRight);
			BottomLeftCommmand = new DelegateCommand(BottomLeft);
			BottomRightCommmand = new DelegateCommand(BottomRight);
			CenterCommmand = new DelegateCommand(Center);
			RandomCellCommmand = new DelegateCommand(RandomCell);

			repository = new BoardRepository();

			ClickPlayer = new MediaPlayer();
			BackgroundPlayer = new MediaPlayer();
			BackgroundPlayer.MediaEnded += BackgroundPlayerOnMediaEnded;
			ClickPlayer.Open(new Uri("Assets/click.mp3", UriKind.Relative));
			BackgroundPlayer.Open(new Uri("Assets/march.mp3", UriKind.Relative));

			LoadState();
		}

		private void BackgroundPlayerOnMediaEnded(object sender, EventArgs eventArgs)
		{
			BackgroundPlayer.Position = new TimeSpan(0);
			if (IsActive)
				BackgroundPlayer.Play();
		}

		private void UpdateEngine()
		{
			engine.DoUpdate();
		}


		private void RandomCell()
		{
			var random = new Random();

			if (LiveCells.Count == 0)
				return;

			var array = LiveCells.ToArray();

			var randomCell = array[random.Next(0, array.Length)];

			var halfWidth = ViewPortWidth / 2;
			var halfHeight = ViewPortHeight / 2;

			XOffset = randomCell.X - halfWidth;
			YOffset = randomCell.Y - halfHeight;

			if (randomCell.X > (long.MaxValue - halfWidth))
				XOffset = (long.MaxValue - ViewPortWidth);

			if (randomCell.X < (long.MinValue + halfWidth))
				XOffset = (long.MinValue);


			if (randomCell.Y > (long.MaxValue - halfHeight))
				YOffset = (long.MaxValue - ViewPortHeight);

			if (randomCell.Y < (long.MinValue + halfHeight))
				YOffset = (long.MinValue);
			
			UpdateEngine();
		}

		private void Center()
		{
			XOffset = 0;
			YOffset = 0;
			UpdateEngine();
		}

		private void BottomRight()
		{
			XOffset = long.MaxValue - GameConfig.Instance.VisibleBoardSizeX;
			YOffset = long.MaxValue - GameConfig.Instance.VisibleBoardSizeY;
			UpdateEngine();
		}

		private void BottomLeft()
		{
			XOffset = long.MinValue;
			YOffset = long.MaxValue - GameConfig.Instance.VisibleBoardSizeY;
			UpdateEngine();
		}

		private void TopRight()
		{
			XOffset = long.MaxValue - GameConfig.Instance.VisibleBoardSizeX;
			YOffset = long.MinValue;
			UpdateEngine();
		}

		private void TopLeft()
		{
			XOffset = long.MinValue;
			YOffset = long.MinValue;
			UpdateEngine();
		}

		public BoardViewModel()
		{
		}

		public long Generation
		{
			get { return engine.Generation; }
			set
			{
				engine.Generation = value;
			}
		}

		private long xOffset = 0;
		public long XOffset
		{
			get { return xOffset; }
			set
			{
				xOffset = value;
				OnPropertyChanged();
			}
		}

		private long yOffset = 0;
		public long YOffset
		{
			get { return yOffset; }
			set
			{
				yOffset = value;
				OnPropertyChanged();
			}
		}

		private long currentCellX = 0;

		public long CurrentCellX
		{
			get { return currentCellX; }
			set
			{
				currentCellX = value;
				OnPropertyChanged();
			}
		}

		private long currentCellY = 0;

		public long CurrentCellY
		{
			get { return currentCellY; }
			set
			{
				currentCellY = value;
				OnPropertyChanged();
			}
		}

		public int ViewPortWidth
		{
			get { return GameConfig.Instance.VisibleBoardSizeX; }
			set
			{
				GameConfig.Instance.VisibleBoardSizeX = value;
				GameConfig.Instance.Save();
				UpdateEngine();
				OnPropertyChanged();
			}
		}

		public int ViewPortHeight
		{
			get { return GameConfig.Instance.VisibleBoardSizeY; }
			set
			{
				GameConfig.Instance.VisibleBoardSizeY = value;
				GameConfig.Instance.Save();
				UpdateEngine();
				OnPropertyChanged();
			}
		}

		void BoardViewModel_Update(object sender)
		{
			BoardViewPort.Update();
			OnPropertyChanged("Generation");
		}

		public HashSet<Position> LiveCells {
			get { return engine.CellsCurrent; }
			set { engine.CellsCurrent = value; }
		}

		public string StartStopText
		{
			get { return IsActive ? "Stop" : "Start"; }
		}

		public bool IsActive
		{
			get { return engine.IsActive; }
			set { engine.IsActive = value; }
		}

		private void StartStop()
		{
			if (IsActive)
			{
				Stop();
			}
			else
			{
				Start();
			}
		}

		public void Start()
		{
			engine.Start();
			BackgroundPlayer.Play();
			OnPropertyChanged("StartStopText");
		}

		public void Stop()
		{
			BackgroundPlayer.Pause();
			engine.Stop();
			OnPropertyChanged("StartStopText");
		}

		public void Clear()
		{
			BackgroundPlayer.Position = new TimeSpan(0);
			engine.Clear();
			XOffset = 0;
			YOffset = 0;
			OnPropertyChanged("StartStopText");
		}


		public void Randomize()
		{
			engine.Randomize(xOffset, yOffset);
			OnPropertyChanged("StartStopText");
		}

		public void Next()
		{
			engine.Next();
		}

		private MediaPlayer ClickPlayer;
		private MediaPlayer BackgroundPlayer;

		public void ToggleCell(long x, long y)
		{
			ClickPlayer.Position = new TimeSpan(0);
			ClickPlayer.Play();
			engine.ToggleCell(x, y);
		}

		private void SaveState()
		{
			var modelState = IsActive;

			Stop();

			repository.SaveBoard("board.txt", LiveCells);

			if (modelState)
				StartStop();
		}

		private void LoadState()
		{
			Stop();
			Clear();

			LiveCells = repository.LoadBoard("board.txt");

			UpdateEngine();
		}
		
		public IBoardView BoardView { get; set; }
		public IBoardViewPort BoardViewPort { get; set; }
		public event PropertyChangedEventHandler PropertyChanged;

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
