﻿using System;
using System.Collections.Generic;
using System.Windows.Threading;
using Conways.GameOfLife.Game;
using Conways.GameOfLife.Infrastructure;

namespace Conways.GameOfLife.Model
{
	public class LifeEngine
	{
		private readonly DispatcherTimer pulse;
		public HashSet<Position> CellsCurrent = new HashSet<Position>();
		public HashSet<Position> CellsNext = new HashSet<Position>();
		public long Generation { get; set; }

		public delegate void OnUpdate(object sender);

		public event OnUpdate Update;
		
		public LifeEngine()
		{
			pulse = new DispatcherTimer { Interval = new TimeSpan(TimeSpan.TicksPerMillisecond * 10) };
			pulse.Tick += pulse_Elapsed;
		}

		private void pulse_Elapsed(object sender, EventArgs e)
		{
			Next();
		}

		public bool IsActive { get; set; }

		public void Start()
		{
			IsActive = true;
			pulse.Start();
		}

		public void Stop()
		{
			IsActive = false;
			pulse.Stop();
		}

		public void Clear()
		{
			if (IsActive)
			{
				Stop();
			}

			CellsCurrent = new HashSet<Position>();
			Generation = 0;

			DoUpdate();
		}

		public void Randomize(long xOffset, long yOffset)
		{
			if (IsActive)
				Stop();

			var r = new Random((int)DateTime.Now.Ticks);

			for (var i = 0; i < GameConfig.Instance.VisibleBoardSizeX * GameConfig.Instance.VisibleBoardSizeY; i++)
			{
				var randomX = RandomHelper.LongRandom(xOffset, xOffset + GameConfig.Instance.VisibleBoardSizeX, r);
				var randomY = RandomHelper.LongRandom(yOffset, yOffset + GameConfig.Instance.VisibleBoardSizeX, r);
				
				var position = new Position(randomX, randomY);

				if (r.Next(0, 2) != 0)
					CellsCurrent.Add(position);
			}

			DoUpdate();
		}

		public void Next()
		{
			foreach (var cell in CellsCurrent)
			{
				var neighbors = GetNeighbours(cell, true);

				if (GetCellState(true, neighbors.LivingCount))
				{
					CellsNext.Add(cell);
				}

				foreach (var deadNeighbor in neighbors.DeadNeighbors)
				{
					var neighborsOfTheDead = GetNeighbours(deadNeighbor, false);

					if (GetCellState(false, neighborsOfTheDead.LivingCount))
					{
						CellsNext.Add(deadNeighbor);
					}
				}
			}

			CellsCurrent = new HashSet<Position>(CellsNext);
			CellsNext = new HashSet<Position>();

			Generation++;

			DoUpdate();
		}

		private static bool GetCellState(bool living, byte neighbors)
		{
			if (living && neighbors < 2)
			{
				living = false;
			}
			else if (living && (2 == neighbors || 3 == neighbors))
			{
				living = true;
			}
			else if (living && neighbors > 3)
			{
				living = false;
			}
			else if (!living && neighbors == 3)
			{
				living = true;
			}

			return living;
		}

		public void ToggleCell(long x, long y)
		{
			var position = new Position(x, y);

			if (CellsCurrent.Contains(position))
				CellsCurrent.Remove(position);
			else
				CellsCurrent.Add(position);

			DoUpdate();
		}

		private Neighbors GetNeighbours(Position position, bool countDead)
		{
			var neighbors = new Neighbors();

			foreach (var pos in GetNeighbotCoords(position))
			{
				if (CellsCurrent.Contains(pos))
				{
					neighbors.LivingCount++;
				}
				else
				{
					if (countDead && !neighbors.DeadNeighbors.Contains(pos))
						neighbors.DeadNeighbors.Add(pos);
				}
			}

			return neighbors;
		}

		public List<Position> GetNeighbotCoords(Position position)
		{
			var neighborsCoords = new List<Position>();

			// Check cell on the right
			neighborsCoords.Add(new Position(position.X + 1, position.Y));

			// Check cell on bottom right
			neighborsCoords.Add(new Position(position.X + 1, position.Y - 1));

			// Check cell on bottom
			neighborsCoords.Add(new Position(position.X, position.Y - 1));

			// Check cell on bottom left
			neighborsCoords.Add(new Position(position.X - 1, position.Y - 1));

			// Check cell on left
			neighborsCoords.Add(new Position(position.X - 1, position.Y));

			// Check cell on top left
			neighborsCoords.Add(new Position(position.X - 1, position.Y + 1));

			// Check cell on top
			neighborsCoords.Add(new Position(position.X, position.Y + 1));

			// Check cell on top right
			neighborsCoords.Add(new Position(position.X + 1, position.Y + 1));

			return neighborsCoords;
		}

		public bool CellAlive(long x, long y)
		{
			return CellsCurrent.Contains(new Position(x, y));
		}

		public void DoUpdate()
		{
			if (null != Update)
			{
				Update(this);
			}
		}
	}
}
