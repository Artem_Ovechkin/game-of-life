﻿using System.Globalization;

namespace Conways.GameOfLife.Game
{
	public struct Position
	{
		public long X;
		public long Y;

		public Position(long x, long y)
		{
			X = x;
			Y = y;
		}

		public override string ToString()
		{
			return string.Format("{0},{1};", X, Y);
		}
	}
}
