﻿using System.Collections.Generic;

namespace Conways.GameOfLife.Game
{
	public class Neighbors
	{
		public byte LivingCount = 0;

		public List<Position> DeadNeighbors = new List<Position>();
	}
}
