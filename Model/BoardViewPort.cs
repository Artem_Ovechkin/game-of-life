﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using Conways.GameOfLife.Game;
using Conways.GameOfLife.Infrastructure;
using Conways.GameOfLife.Interfaces;

namespace Conways.GameOfLife.Model
{
	public class BoardViewPort : FrameworkElement, IBoardViewPort
	{
		public IBoardViewModel ViewModel { get; set; }
		
		private const double OUTLINE_WIDTH = 1;

		private readonly DrawingVisual[] visuals;

		private readonly DrawingVisual grid = new DrawingVisual();

		private readonly DrawingVisual cells = new DrawingVisual();

		Rect cell = new Rect(OUTLINE_WIDTH, OUTLINE_WIDTH, GameConfig.Instance.CellSize - OUTLINE_WIDTH, GameConfig.Instance.CellSize - OUTLINE_WIDTH);
		
		public BoardViewPort()
		{
			AddVisualChild(grid);
			AddLogicalChild(grid);

			AddVisualChild(cells);
			AddLogicalChild(cells);

			visuals = new[] { grid, cells };
		}

	
		public void Clear()
		{
			DrawGrid();
		}

		protected override int VisualChildrenCount
		{
			get
			{
				return visuals.Length;
			}
		}

		protected override Visual GetVisualChild(int index)
		{
			if (index < 0 || index > visuals.Length)
			{
				throw new ArgumentOutOfRangeException("index");
			}

			return visuals[index];
		}

		protected override Size MeasureOverride(Size availableSize)
		{
			return new Size(GameConfig.Instance.VisibleBoardSizeX * GameConfig.Instance.CellSize, GameConfig.Instance.VisibleBoardSizeY * GameConfig.Instance.CellSize);
		}

		protected override void OnRender(DrawingContext drawingContext)
		{
			DrawGrid();
			DrawCells();
		}

		protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
		{
			base.OnMouseLeftButtonDown(e);
			
			var pt = e.GetPosition(this);
			var x = (long)((pt.X) / GameConfig.Instance.CellSize) + ViewModel.XOffset;
			var y = (long)((pt.Y) / GameConfig.Instance.CellSize) + ViewModel.YOffset;

			ViewModel.ToggleCell(x, y);
		}

		private Point DragStartPoint;

		protected override void OnMouseRightButtonDown(MouseButtonEventArgs e)
		{
			base.OnMouseRightButtonDown(e);

			var pt = e.GetPosition(this);
			DragStartPoint.X = pt.X;
			DragStartPoint.Y = pt.Y;
		}

		protected override void OnMouseMove(MouseEventArgs e)
		{
			base.OnMouseMove(e);

			var pt = e.GetPosition(this);

			var xOffset = (int)((DragStartPoint.X - pt.X) / GameConfig.Instance.CellSize);
			var yOffset = (int)((DragStartPoint.Y - pt.Y) / GameConfig.Instance.CellSize);

			if (xOffset == 0 && yOffset == 0)
				return;

			 if (e.RightButton != MouseButtonState.Pressed)
			{
				if (!ViewModel.IsActive)
				{
					var x = (long)((Mouse.GetPosition(this).X) / GameConfig.Instance.CellSize) + ViewModel.XOffset;
					var y = (long)((Mouse.GetPosition(this).Y) / GameConfig.Instance.CellSize) + ViewModel.YOffset;

					SetCurrentCell(x, y);
				}
				return;
			}

			if (!IsXLimit(xOffset))
				ViewModel.XOffset += xOffset;
			if (!IsYLimit(yOffset))
				ViewModel.YOffset += yOffset;

			DragStartPoint = new Point(pt.X, pt.Y);
		
			Update();
		}

		private void SetCurrentCell(long x, long y)
		{
			ViewModel.CurrentCellX = x;
			ViewModel.CurrentCellY = y;
		}


		private bool IsXLimit(int offset)
		{
			if (offset > 0)
				return ((ViewModel.XOffset + offset + ViewModel.ViewPortWidth) < ViewModel.XOffset);

			return (ViewModel.XOffset + offset > ViewModel.XOffset);
		}

		private bool IsYLimit(int offset)
		{
			if (offset > 0)
				return ((ViewModel.YOffset + offset + ViewModel.ViewPortHeight) < ViewModel.YOffset);

			return (ViewModel.YOffset + offset > ViewModel.YOffset);
		}

		private Pen outline = new Pen(Brushes.LightGray, OUTLINE_WIDTH);

		private void DrawGrid()
		{
			var background = new Rect(0, 0, GameConfig.Instance.CellSize * GameConfig.Instance.VisibleBoardSizeX, GameConfig.Instance.CellSize * GameConfig.Instance.VisibleBoardSizeY);

			using (var dc = grid.RenderOpen())
			{
				dc.DrawRectangle(Brushes.Gray, null, background);

				var start = new Point(0, 0);
				var end = new Point(0, background.Bottom);
				for (int i = 0; i < ViewModel.ViewPortWidth; i++)
				{
					dc.DrawLine(outline, start, end);
					start.Offset(GameConfig.Instance.CellSize, 0);
					end.Offset(GameConfig.Instance.CellSize, 0);
				}

				start = new Point(0, 0);
				end = new Point(background.Right, 0);
				for (int i = 0; i < ViewModel.ViewPortWidth; i++)
				{
					dc.DrawLine(outline, start, end);
					start.Offset(0, GameConfig.Instance.CellSize);
					end.Offset(0, GameConfig.Instance.CellSize);
				}
			}
		}

		private void DrawCells()
		{
			if (ViewModel == null)
				return;
			using (var dc = cells.RenderOpen())
			{
				foreach (var liveCell in ViewModel.LiveCells)
				{
					var minX = ViewModel.XOffset;
					var maxX = ViewModel.XOffset + GameConfig.Instance.VisibleBoardSizeX - 1;
					var minY = ViewModel.YOffset;
					var maxY = ViewModel.YOffset + GameConfig.Instance.VisibleBoardSizeY - 1;
					var cellX = liveCell.X;
					var cellY = liveCell.Y;

					if ((cellX <= maxX && cellX >= minX) && (cellY <= maxY && cellY >= minY))
					{
						cell.Location = new Point(((cellX - ViewModel.XOffset) * GameConfig.Instance.CellSize) + OUTLINE_WIDTH, ((cellY - ViewModel.YOffset) * GameConfig.Instance.CellSize) + OUTLINE_WIDTH);

						dc.DrawRectangle(Brushes.Red, null, cell);
					}

				}
			}
		}


		public void Update()
		{
			var x = (long)((Mouse.GetPosition(this).X) / GameConfig.Instance.CellSize) + ViewModel.XOffset;
			var y = (long)((Mouse.GetPosition(this).Y) / GameConfig.Instance.CellSize) + ViewModel.YOffset;

			SetCurrentCell(x, y);

			DrawGrid();
			DrawCells();
		}
	}
}